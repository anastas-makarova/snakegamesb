// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_Apple_generated_h
#error "Apple.generated.h already included, missing '#pragma once' in Apple.h"
#endif
#define SNAKEGAME_Apple_generated_h

#define SnakeGame_Source_SnakeGame_Apple_h_12_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_Apple_h_12_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_Apple_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_Apple_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAApple(); \
	friend struct Z_Construct_UClass_AApple_Statics; \
public: \
	DECLARE_CLASS(AApple, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AApple)


#define SnakeGame_Source_SnakeGame_Apple_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAApple(); \
	friend struct Z_Construct_UClass_AApple_Statics; \
public: \
	DECLARE_CLASS(AApple, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AApple)


#define SnakeGame_Source_SnakeGame_Apple_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AApple(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AApple) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AApple); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AApple); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AApple(AApple&&); \
	NO_API AApple(const AApple&); \
public:


#define SnakeGame_Source_SnakeGame_Apple_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AApple(AApple&&); \
	NO_API AApple(const AApple&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AApple); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AApple); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AApple)


#define SnakeGame_Source_SnakeGame_Apple_h_12_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_Apple_h_9_PROLOG
#define SnakeGame_Source_SnakeGame_Apple_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Apple_h_12_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Apple_h_12_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_Apple_h_12_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_Apple_h_12_INCLASS \
	SnakeGame_Source_SnakeGame_Apple_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_Apple_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Apple_h_12_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Apple_h_12_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_Apple_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Apple_h_12_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Apple_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AApple>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_Apple_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
