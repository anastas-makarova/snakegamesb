
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Math/UnrealMathUtility.h"
#include "PlayerPawnBase.h"


ASnakeBase::ASnakeBase()
{
 	
	PrimaryActorTick.bCanEverTick = true;
	//ElementSize = 100.f;
	//MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(3);

	PlayerPawnBase = Cast<AplayerPawnBase>(GetInstigatorController() - GetPawn();



	
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	SetActorTickInterval(MovementSpeed);
	MovementSpeed = FMath::Clamp(MovementSpeed, 0.2f, 0.8f);
	
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	FVector PrevLocation = FVector::ZeroVector;

	for (int i = 0; i < ElementsNum; ++i)
	{
		for (int j = SnakeElements.Num() - 1; j > 0; j--)
		{
			PrevElement = SnakeElements[j];
			PrevLocation = PrevElement->GetActorLocation();
		}

		FVector NewLocation(SnakeElements.Num()*ElementSize, 0, 0);
		FTransform NewTranform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor <ASnakeElementBase>(SnakeElementClass, NewTranform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
			
		}
		if (ElemIndex != 0 && ElemIndex % 2 == 0)
		{
			SnakeElements[ElemIndex]->GetMesh()->SetMaterial(0, ElementMaterial);
		}

	}

float ASnakeBase::GetMovemenSpeed()
{
	return MovementSpeed;
}

float ASnakeBase::SetMovementSpeed(float NewMovementSpeed)
{
	return MovementSpeed= NewMovementSpeed;
}

void ASnakeBase::SetIsAbeleMove(bool IsValue)
{
	IsMoveProgress = IsValue;
}

void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);

	switch(LastMoveDirection)
	{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			break;
	}


	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0;i--)
	{
		/*SnakeElements[0]->SetActorHiddenInGame(false);
		SnakeElements[i]->SetActorHiddenInGame(false);*/
		auto CurrentElement = SnakeElements[i];
		PrevElement = SnakeElements[i - 1];

		auto BaseRot = PrevElement->GetActorRotation();
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	SetIsAbeleMove(false);
	

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;

		IInteractable* InteractableInterface = Cast <IInteractable>(Other);
		if (InteractableInterface) 
		{
			InteractableInterface->Interact(this, bIsFirst);
		}

	}
}


