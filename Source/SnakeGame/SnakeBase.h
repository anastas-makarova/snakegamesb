// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"
#include "PlayerPawnBase.h"


class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

public:
	ASnakeBase();


	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

		bool InPortal = false;

protected:
	virtual void BeginPlay() override;

	bool IsMoveProgress = false;

	UPROPERTY()
	ASnakeElementbase* PrevElement = nullptr;


	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* ElementMaterial;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed = 0.4f;

	UPROPERTY()
	APlayerPawnBase* PlayerPawnBase;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;


public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);


	UFUNCTION(BlueprintCallable)
	float GetMovementSpeed() const;

	UFUNCTION()
	float SetMovementSpeed(float NewMovementSpeed);

	UFUNCTION()
	void SetIsAbeleMove(bool IsValue);

	UFUNCTION(BlueprintCallable)
	void Move();

	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	/*FORCEINLINE ASnakeElementBase* GetPrevElement() const
	{
		return PrevElement;
	}

	FORCEINLINE TArray<ASnakeElementBase*> GetSnakeElements()
	{
		return SnakeElements;
	}

	FORCEINLINE bool GetIsAbelMove()
	{
		return IsMoveProgress;
	}
	*/


};
