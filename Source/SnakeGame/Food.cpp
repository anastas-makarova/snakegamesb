// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//StaticMeshComp = CreateDefaultSubobject <UStaticMeshComponent>("MeshComp"););
	//RootComponent = StaticMeshComp;
//StaticMeshComp->SetRelativeScale3D(FVector(0.3f, 0.3f, 0.3f));

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{ 
		auto Snake = Cast <ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy(true, true);
		}
		

	}

}

/*void AFood::EatFood(ASnakeBase* SnakeBase, float SpeedBonus, int AddElement)
{
	SnakeBase->AddSnakeElement(AddElement);
	SnakeBase->SetMovementSpeed(SpeedBonus);

}*/